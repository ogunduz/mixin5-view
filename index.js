
module.exports = View;

var inherits = require('mixin5-inherits');

function View (obj) {
  if (obj) return inherits(obj, View);
}

View.prototype.foo = function () {
  return 'bar';
};
